<?php

class Messages extends Admin_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model(array('admin/message'));
		$this->load->model(array('admin/label'));
		$this->load->model(array('admin/gmail_user'));
    }

    public function index() {
		
		$emails = $this->gmail_user->get_all();		
		
		$data = array(
			'emails' 	=> $emails
		);
		
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "messages_list";
        $this->load->view($this->_container, $data);
    }
	
	public function getLastWeek($dateD){
		$day = date('w', strtotime("-1 week")); 
		$week_start = date('Y-m-d', strtotime('-'.$day.' days'));
		$week_end = date('Y-m-d', strtotime('+'.(6-$day).' days'));
		
		$week_start = date('Y-m-d', strtotime("$week_start -1 week"));
		$week_end = date('Y-m-d', strtotime("$week_end -1 week"));
		
		$data = ($dateD == 'dateF') ? $week_start : $week_end;
		
		return $data;
	}
	
	public function account($id){
		$emails = $this->gmail_user->get_all();
		
		
		$dateFrom = (!empty($this->input->post('dateFrom'))) ? date('Y-m-d', strtotime($this->input->post('dateFrom'))) : $this->getLastWeek('dateF');
		$dateTo = (!empty($this->input->post('dateTo'))) ? date('Y-m-d', strtotime($this->input->post('dateTo'))) : $this->getLastWeek('dateT');
		 
		if($id){			
			 
			$messages = $this->message->get_message_user($id);		
			$emailId = $this->gmail_user->get($id);
			
			$data['messages'] = $messages; 
							
			//count # of tags
			 
			foreach($messages as $list){
				
				$labelArray = explode(',', $list['label_id']);
				foreach($labelArray as $tag_id){
					$msgByDate[$list['message_date']][] = $this->getLabelName($id, $tag_id);
					
					if($tag_id == 'SENT'){
						$msgSent[$list['message_date']][$list['thread_id']][] = $list['message_id'];
						 
						$messageTo = substr($list['message_to'], strpos($list['message_to'], "@")+1); 
						$companyContacted[$list['message_date']][$messageTo] = $messageTo;
					}
				}
				
			} 
			//var_dump('<pre>', $msgByDate);die();
			
			$auto 			= 0;
			$bounce 		= 0;
			$forwarded 		= 0;
			$hot_call 		= 0;
			$hot_more 		= 0;
			$not_interested = 0;
			$referral 		= 0;
			$reply 			= 0;
			
			$sentTotal 		= 0;
			$contactTotal	= 0;
			$autoTotal 		= 0;
			$bounceTotal 	= 0;
			$forwardedTotal = 0;
			$hot_callTotal 	= 0;
			$hot_moreTotal 	= 0;
			$not_interestedTotal = 0;
			$referralTotal 	= 0;
			$reply 			= 0;
			$replyTotal 	= 0;
			$sentResponse 	= 0;
			$response 		= 0;
			
			$msg = array();
			
			$hotLeadCall = strtolower('HOT LEAD (Call)');
			$hotLeadMore = strtolower('HOT LEAD (More Info)');
			 
			foreach($msgByDate as $date => $val){
				
				$dateMsg	= strtotime($date);
				$dateF		= strtotime($dateFrom);
				$dateT		= strtotime($dateTo);
				
				if($dateMsg >= $dateF && $dateMsg <= $dateT){
					
					foreach($val as $label){
						 
						$msg[$date]['AUTO REPLY'] = (strtolower($label) == strtolower('AUTO REPLY')) ? $auto += 1 : $auto;
						$msg[$date]['BOUNCE BACK'] = (strtolower($label) == strtolower('BOUNCE BACK')) ? $bounce += 1 : $bounce;
						$msg[$date]['FORWARDED TO SALES'] = (strtolower($label) == strtolower('FORWARDED TO SALES')) ? $forwarded += 1 : $forwarded;
						$msg[$date]['HOT LEAD (Call)'] = (strtolower($label) == strtolower('HOT LEAD (Call)')) ? $hot_call += 1 : $hot_call;
						$msg[$date]['HOT LEAD (More Info)'] = (strtolower($label) == strtolower('HOT LEAD (More Info)')) ? $hot_more += 1 : $hot_more;
						$msg[$date]['NOT INTERESTED'] = (strtolower($label) == strtolower('NOT INTERESTED')) ? $not_interested += 1 : $not_interested;
						$msg[$date]['REFERRAL'] = (strtolower($label) == strtolower('REFERRAL')) ? $referral += 1 : $referral;
						
						
						if( strtolower($label) == strtolower('FORWARDED TO SALES') 
							|| strtolower($label) == strtolower('HOT LEAD (Call)') || $label == strtolower('HOT LEAD (More Info)') 
							|| strtolower($label) == strtolower('NOT INTERESTED') || strtolower($label) == strtolower('REFERRAL') ){ 
							$msg[$date]['REPLY'] = $reply += 1;
						}else{
							$msg[$date]['REPLY'] = $reply;
						}
					}
					 
					$msg[$date]['SENT'] = (isset($msgSent[$date])) ? count($msgSent[$date]) : 0;  
					$msg[$date]['CONTACTED'] = (isset($companyContacted[$date])) ? count($companyContacted[$date]) : 0;
					
					
					$msgTotal['SENT'] = $sentTotal += $msg[$date]['SENT'];
					$msgTotal['CONTACTED'] = $contactTotal += $msg[$date]['CONTACTED'];
					$msgTotal['AUTO REPLY'] = $autoTotal += $msg[$date]['AUTO REPLY'];
					$msgTotal['BOUNCE BACK'] = $bounceTotal += $msg[$date]['BOUNCE BACK'];
					$msgTotal['FORWARDED TO SALES'] = $forwardedTotal += $msg[$date]['FORWARDED TO SALES'];
					$msgTotal['HOT LEAD (Call)'] = $hot_callTotal += $msg[$date]['HOT LEAD (Call)'];
					$msgTotal['HOT LEAD (More Info)'] = $hot_moreTotal += $msg[$date]['HOT LEAD (More Info)'];
					$msgTotal['NOT INTERESTED'] = $not_interestedTotal += $msg[$date]['NOT INTERESTED'];
					$msgTotal['REFERRAL'] = $referralTotal += $msg[$date]['REFERRAL'];					
					$response = $replyTotal += $msg[$date]['REPLY'];
					$sentResponse = $msgTotal['SENT'];
					 
					
					//refresh value
					$auto 			= 0;
					$bounce 		= 0;
					$forwarded 		= 0;
					$hot_call 		= 0;
					$hot_more 		= 0;
					$not_interested = 0;
					$referral 		= 0;
					$reply 			= 0;
				} 
			
			}
			
			$msgTotal['RESPONSE'] = ($sentResponse !=0) ? round ( $response / $sentResponse, 2)*100 : 0;
			$msgTotal['BOUNCE RATE'] = ($sentResponse !=0) ? round ( $msgTotal['BOUNCE BACK'] / $sentResponse, 2)*100 : 0 ;
			
			//var_dump('<pre>', $msgTotal);die();
			 
			//
			$data['msg'] = $msg;		 
		}
		
		$data = array(
			'emails' 	=> $emails,
			'dateFrom' 	=> $dateFrom,
			'dateTo'	=> $dateTo,
			'msg'		=> $msg,
			'msgTotal'	=> $msgTotal,
			'emailId'	=> $emailId
		);
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "messages_account_list";
        $this->load->view($this->_container, $data);
	}
	
	public function getLabelName($gmailUserId, $labelId){
		$labels = $this->label->get_labels_user($gmailUserId);
		
		foreach($labels as $key => $val){
			if($labelId == $val['id']){
				$labelName = $val['name'];
			}
		}
		
		return $labelName;
	}
	
    public function create() {
        if ($this->input->post('description')) {
            $data['description'] = $this->input->post('description');
            $this->category->insert($data);

            redirect('/admin/categories', 'refresh');
        }

        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "categories_create";
        $this->load->view($this->_container, $data);
    }

    public function edit($id) {
        if ($this->input->post('description')) {
            $data['description'] = $this->input->post('description');
            $this->category->update($data, $id);

            redirect('/admin/categories', 'refresh');
        }

        $category = $this->category->get($id);

        $data['category'] = $category;
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "categories_edit";
        $this->load->view($this->_container, $data);
    }

    public function delete($id) {
        $this->category->delete($id);

        redirect('/admin/categories', 'refresh');
    }

}
