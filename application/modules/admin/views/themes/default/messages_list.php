<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header users-header">
                <h2>
                    Messages
                    
                </h2>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
	
	<div class="row">
        <div class="col-lg-12">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
							<td>Email Accounts:</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
					<?php if (count($emails)): ?>
						<?php foreach ($emails as $key => $user): ?>
						<tr>
							<td><a href="<?= base_url("admin/messages/account") ?>/<?=$user['id']?>"><?=$user['email']?></a></td>
							<td><a href="<?= base_url("admin/messages/account") ?>/<?=$user['id']?>" class="btn btn-info">view</a></td>
						</tr>
						<?php endforeach; ?>
					<?php endif; ?>	
					</tbody>
				</table>
				 				
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
	
    <!-- /.row -->
    
</div>
</div>
<!-- /#page-wrapper -->