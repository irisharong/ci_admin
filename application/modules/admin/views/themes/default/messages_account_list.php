<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header users-header">
                <h2>
                    Clients
                    
                </h2>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
	
	<div class="row">
        <div class="col-lg-12">
            <div>
                <p> Email Accounts:  </p>
				<?php if (count($emails)): ?> 
					<div class="dropdown">
					  <button class="btn btn-default dropdown-toggle" type="button" id="emails" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						Emails
						<span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu" aria-labelledby="emails">
						<?php foreach ($emails as $key => $user): ?>
							<li><a href="<?= base_url("admin/messages/account") ?>/<?=$user['id']?>"><?=$user['email']?></a></li>
						<?php endforeach; ?>				
						 
					  </ul>
					</div>
	  
				<?php endif; ?>					
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
	<br/>
	<div class="row">
		<div class="col-lg-12">
            <div> 
				<form method="post" action="">
				   <span>Date from: <input type="text" name="dateFrom" id="dateFrom" value="<?=$dateFrom?>"/></span>
				   <span>Date to: <input type="text" name="dateTo" id="dateTo" value="<?=$dateTo?>"/></span>
				   <button type="submit" class="btn btn-default">Submit Date</button>
				</form>				
            </div>
			
        </div>
        <!-- /.col-lg-12 -->
		
	</div>
	<br/> 
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Messages listing for <strong><?php echo $email = isset($emailId) ? $emailId->email : ""?></strong>
                </div>
				
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- display messages count -->
					
					
					<div class="dataTable_wrapper">
                                       <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
									<th>Date</th>
									<th>Emails Sent</th>
									<th>Companies Contacted</th>
									<th>Auto Reply</th>
                                    <th>Bounce Back</th>
                                    <th>Forwarded to Sales</th>
                                    <th>Hot Lead (Call)</th>
                                    <th>Hot Lead (More Info)</th>
                                    <th>Not Interested</th>
									<th>Referral</th>				
									<th>Response Rate</th>				
									<th>Bounce Rate</th>								 
                                </tr>
                            </thead>
                            <tbody>
								<?php if (count($msg)): ?>
                                    <?php foreach ($msg as $date => $label): ?>
                                    <tr>
										<td><?=$date?></td>
										<td><?=$label['SENT']?></td>
										<td><?=$label['CONTACTED']?></td>
										<td><?=$label['AUTO REPLY']?></td>
										<td><?=$label['BOUNCE BACK']?></td>
										<td><?=$label['FORWARDED TO SALES']?></td>
										<td><?=$label['HOT LEAD (Call)']?></td>
										<td><?=$label['HOT LEAD (More Info)']?></td>
										<td><?=$label['NOT INTERESTED']?></td>
										<td><?=$label['REFERRAL']?></td>
										<?php if ($label['SENT'] != 0): ?>
											<td><?=round($label['REPLY'] / $label['SENT'], 2)*100 ?></td>										
											<td><?=round($label['BOUNCE BACK'] / $label['SENT'], 2)*100 ?></td>
										 
										<?php else : ?>
											<td><?=$label['SENT']?></td>										
											<td><?=$label['SENT']?></td>
										<?php endif; ?>
									</tr>
									<?php endforeach; ?>
								<?php endif; ?>
								
								 
                            </tbody>
                            <tfooter>
                                <tr>
									<td><b>Total</b></td>
								<?php if (count($msgTotal)): ?>
                                    <?php foreach ($msgTotal as $label => $val): ?>
                                    										
										<td><strong><?=$val?></strong></td>										 
									
									<?php endforeach; ?>
								<?php endif; ?>
								</tr> 
                            </tfooter>
                        </table>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
</div>
<!-- /#page-wrapper -->