<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Label extends MY_Model {

    public function __construct() {
        parent::__construct();
    }
	
	public function get_labels_user($gmaiUserId) {
        $data = $this->db->get_where($this->table_name, array('gmail_user_id' => $gmaiUserId));
		
		if ($data->num_rows() > 0) {
			$cnt = 0;
            foreach ($data->result_array() as $row) { 
                $labels[$cnt]['name'] = $row['label_name'];				
                $labels[$cnt]['id'] = $row['label_id'];
				
				$cnt++;
            }
        }
        $data->free_result();

        return $labels;
    }
	
}
