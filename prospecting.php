<?php
require __DIR__ . '/vendor/autoload.php';

define('APPLICATION_NAME', 'Gmail API PHP Quickstart');
//define('CREDENTIALS_PATH',  __DIR__ . '/credentials/gmail-php-quickstart-newsletter.json');
//define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret/client_secret_newsletter.json');
// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/gmail-php-quickstart.json
define('SCOPES', implode(' ', array(
  Google_Service_Gmail::GMAIL_READONLY)
));

if (php_sapi_name() != 'cli') {
  throw new Exception('This application must be run on the command line.');
}

//DB connection_aborted
$servername = "localhost";
$username = "root";
$password = "";
$db = "ci_my_admin";

// Create connection
$db = new mysqli($servername, $username, $password, $db);

// Check connection
if ($db->connect_error) {
    die("Connection failed: " . $db->connect_error);
} 
echo "Connected successfully";

//loop gmail user to run the script for each email
$sql = "SELECT * FROM gmail_users";
$result = $db->query($sql);

if ($result->num_rows > 0) {			
	while($row = $result->fetch_assoc()) { 
		$clientEmail[] = $row; 
    }
} else {
    echo "0 results";
}


foreach($clientEmail as $prospect => $val){
	
	// Get the API client and construct the service object.	 
	$client = getClient($val['client_secret'], $val['credential_path']); 
	$service = new Google_Service_Gmail($client);

	// Print the labels in the user's account. 
	$user = 'me';
var_dump($val['client_secret']);
	$listMsgs = listMessages($service, $user);
	
	//save to db
	foreach($listMsgs as $lists){ 

		//remove quotes or extra character
		$sentTo = $lists['sentTo'];
		$sentTo = str_replace("'", "", $sentTo);
		$sentTo = str_replace('"', '', $sentTo);
		$sentTo = str_replace('<', '', $sentTo);
		$sentTo = str_replace('>', '', $sentTo);
		
		$label = implode(",", $lists['labels']);
		
		$sql = "INSERT INTO messages (gmail_user_id, message_id, label_id, thread_id, message_to, message_date)
			VALUES ('".$val['id']."', '".$lists['id']."', '".$label."', '".$lists['thread']."', '".$sentTo."', '".$lists['date']."')";
			
		if ($db->query($sql) === TRUE) {
			echo "New record created successfully";
		}else{
			trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $db->error, E_USER_ERROR);
		}
	
	}
}



//db close	 
$db->close(); 

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient($clientSecret, $credentialPath) {
	
  $credential_path = __DIR__ . '/credentials/gmail-php-quickstart-'.$credentialPath.'.json';	
  $client_secret_path = __DIR__ . '/client_secret/client_secret_'.$clientSecret.'.json';
   
  $client = new Google_Client();
  $client->setApplicationName(APPLICATION_NAME);
  $client->setScopes(SCOPES);
  $client->setAuthConfigFile($client_secret_path);
  $client->setAccessType('offline');

  // Load previously authorized credentials from a file.
  $credentialsPath = expandHomeDirectory($credential_path);
  if (file_exists($credentialsPath)) {
    $accessToken = file_get_contents($credentialsPath);
  } else {
    // Request authorization from the user.
    $authUrl = $client->createAuthUrl();
    printf("Open the following link in your browser:\n%s\n", $authUrl);
    print 'Enter verification code: ';
    $authCode = trim(fgets(STDIN));

    // Exchange authorization code for an access token.
    $accessToken = $client->authenticate($authCode);

    // Store the credentials to disk.
    if(!file_exists(dirname($credentialsPath))) {
      mkdir(dirname($credentialsPath), 0700, true);
    }
    file_put_contents($credentialsPath, $accessToken);
    printf("Credentials saved to %s\n", $credentialsPath);
  }
  $client->setAccessToken($accessToken);

  // Refresh the token if it's expired.
  if ($client->isAccessTokenExpired()) {
    $client->refreshToken($client->getRefreshToken());
    file_put_contents($credentialsPath, $client->getAccessToken());
  }
  return $client;
}

/**
 * Expands the home directory alias '~' to the full path.
 * @param string $path the path to expand.
 * @return string the expanded path.
 */
function expandHomeDirectory($path) {
  $homeDirectory = getenv('HOME');
  if (empty($homeDirectory)) {
    $homeDirectory = getenv("HOMEDRIVE") . getenv("HOMEPATH");
  }
  return str_replace('~', realpath($homeDirectory), $path);
}

function listMessages($service, $userId) {
  $pageToken = NULL;
  $messages = array();
  //_param = array('q'=>'after:2016/05/17 -label:draft ');  //date should be 1week earlier
	$opt_param = array('q'=>'after:2016/05/30 -label:draft ');
  //$opt_param = array('q'=>'-label:draft ');
  //$opt_param = array('q'=>'in:sent');
  do {
    try {
      if ($pageToken) {
        $opt_param['pageToken'] = $pageToken;
      }
      $messagesResponse = $service->users_messages->listUsersMessages($userId, $opt_param);
      if ($messagesResponse->getMessages()) {
        $messages = array_merge($messages, $messagesResponse->getMessages());
        $pageToken = $messagesResponse->getNextPageToken();
      }
    } catch (Exception $e) {
      print 'An error occurred: ' . $e->getMessage();
    }
  } while ($pageToken);
var_dump(count($messages));
  $index = 0;
  foreach ($messages as $message) {
	 
    $msgs = $service->users_messages->get($userId, $message['id']);
	  
	if(!in_array("DRAFT", $msgs['labelIds'])){
		$msgLabels[$index]['id'] = $message['id'];
	
		$epoch = $msgs['internalDate'];
		$seconds = $epoch / 1000;	
		$msgLabels[$index]['date'] = date("Y-m-d", $seconds);
		
		$msgLabels[$index]['labels'] = $msgs['labelIds'];
		$msgLabels[$index]['thread'] = $msgs['threadId'];
		
		$msgHeaders = $msgs['modelData']['payload']['headers'];
		$msgSentTo = '';
		foreach($msgHeaders as $key => $val){ 
			if($val['name'] == 'To'){
				$msgSentTo = $val['value'];
			}
		}
		
		$msgLabels[$index]['sentTo'] = $msgSentTo;

	}
	
	$index++; 
	 var_dump($index);
  }
  
  return $msgLabels;
}

