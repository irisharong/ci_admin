<?php  /*    RUN THIS FOR NEWLY REGISTERED EMAIL   */


require __DIR__ . '/vendor/autoload.php';

define('APPLICATION_NAME', 'Gmail API PHP Quickstart');
define('CREDENTIALS_PATH',  __DIR__ . '/credentials/gmail-php-quickstart-newsletter.json');
define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret/client_secret_newsletter.json');
// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/gmail-php-quickstart.json
define('SCOPES', implode(' ', array(
  Google_Service_Gmail::GMAIL_READONLY)
));

if (php_sapi_name() != 'cli') {
  throw new Exception('This application must be run on the command line.');
}

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient() {
  $client = new Google_Client();
  $client->setApplicationName(APPLICATION_NAME);
  $client->setScopes(SCOPES);
  $client->setAuthConfigFile(CLIENT_SECRET_PATH);
  $client->setAccessType('offline');

  // Load previously authorized credentials from a file.
  $credentialsPath = expandHomeDirectory(CREDENTIALS_PATH);
  if (file_exists($credentialsPath)) {
    $accessToken = file_get_contents($credentialsPath);
  } else {
    // Request authorization from the user.
    $authUrl = $client->createAuthUrl();
    printf("Open the following link in your browser:\n%s\n", $authUrl);
    print 'Enter verification code: ';
    $authCode = trim(fgets(STDIN));

    // Exchange authorization code for an access token.
    $accessToken = $client->authenticate($authCode);

    // Store the credentials to disk.
    if(!file_exists(dirname($credentialsPath))) {
      mkdir(dirname($credentialsPath), 0700, true);
    }
    file_put_contents($credentialsPath, $accessToken);
    printf("Credentials saved to %s\n", $credentialsPath);
  }
  $client->setAccessToken($accessToken);

  // Refresh the token if it's expired.
  if ($client->isAccessTokenExpired()) {
    $client->refreshToken($client->getRefreshToken());
    file_put_contents($credentialsPath, $client->getAccessToken());
  }
  return $client;
}

/**
 * Expands the home directory alias '~' to the full path.
 * @param string $path the path to expand.
 * @return string the expanded path.
 */
function expandHomeDirectory($path) {
  $homeDirectory = getenv('HOME');
  if (empty($homeDirectory)) {
    $homeDirectory = getenv("HOMEDRIVE") . getenv("HOMEPATH");
  }
  return str_replace('~', realpath($homeDirectory), $path);
}

function listMessages($service, $userId) {
  $pageToken = NULL;
  $messages = array();
  //_param = array('q'=>'after:2016/05/17 -label:draft ');  //date should be 1week earlier
	//$opt_param = array('q'=>'before:2016/05/31 -label:draft ');
	$opt_param = array('q'=>'after:2016/05/30 -label:draft ');
 // $opt_param = array('q'=>'-label:draft ');
  //$opt_param = array('q'=>'in:sent');
  do {
    try {
      if ($pageToken) {
        $opt_param['pageToken'] = $pageToken;
      }
      $messagesResponse = $service->users_messages->listUsersMessages($userId, $opt_param);
      if ($messagesResponse->getMessages()) {
        $messages = array_merge($messages, $messagesResponse->getMessages());
        $pageToken = $messagesResponse->getNextPageToken();
      }
    } catch (Exception $e) {
      print 'An error occurred: ' . $e->getMessage();
    }
  } while ($pageToken);
var_dump(count($messages));
  $index = 0;
  foreach ($messages as $message) {
	 
    $msgs = $service->users_messages->get($userId, $message['id']);
	  
	if(!in_array("DRAFT", $msgs['labelIds'])){
		$msgLabels[$index]['id'] = $message['id'];
	
		$epoch = $msgs['internalDate'];
		$seconds = $epoch / 1000;	
		$msgLabels[$index]['date'] = date("Y-m-d", $seconds);
		
		$msgLabels[$index]['labels'] = $msgs['labelIds'];
		$msgLabels[$index]['thread'] = $msgs['threadId'];
		
		$msgHeaders = $msgs['modelData']['payload']['headers'];
		$msgSentTo = '';
		foreach($msgHeaders as $key => $val){ 
			if($val['name'] == 'To'){
				$msgSentTo = $val['value'];
			}
		}
		
		$msgLabels[$index]['sentTo'] = $msgSentTo;

	}
	
	$index++; 
	 var_dump($index);
  }
  
  return $msgLabels;
}

//DB connection_aborted
$servername = "localhost";
$username = "root";
$password = "";
$db = "ci_my_admin";

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
echo "Connected successfully";


// Get the API client and construct the service object.
$client = getClient();
$service = new Google_Service_Gmail($client);

// Print the labels in the user's account.
//save labels by email
$user = 'me';

$listMsgs = listMessages($service, $user);
//var_dump('<pre>', $listMsgs); die();

//<add code here> --> check if labels are not save in db
/*
$results = $service->users_labels->listUsersLabels($user);
if (count($results->getLabels()) == 0) {
  print "No labels found.\n";
} else {
   
  foreach ($results->getLabels() as $label) {
     
	$sql = "INSERT INTO labels (gmail_user_id, label_name, label_id)
			VALUES (11, '".$label->getName()."', '".$label->getId()."')";

	if ($conn->query($sql) === TRUE) {
		echo "New record created successfully";
	}else{
		trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
	}
  }
}*/
//save to db

foreach($listMsgs as $lists){ 

		//remove quotes
		$sentTo = $lists['sentTo'];
		$sentTo = str_replace("'", "", $sentTo);
		$sentTo = str_replace('"', '', $sentTo);
		$sentTo = str_replace('<', '', $sentTo);
		$sentTo = str_replace('>', '', $sentTo);
		
		$label = implode(",", $lists['labels']);
		
		$sql = "INSERT INTO messages (gmail_user_id, message_id, label_id, thread_id, message_to, message_date)
			VALUES (11, '".$lists['id']."', '".$label."', '".$lists['thread']."', '".$sentTo."', '".$lists['date']."')";
			
		if ($conn->query($sql) === TRUE) {
			echo "New record created successfully";
		}else{
			trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
		}
	
}

//db close	 
$conn->close();
