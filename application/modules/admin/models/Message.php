<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Message extends MY_Model {

    public function __construct() {
        parent::__construct();
    }
	
    public function get_message_user($gmaiUserId = null) { 
		if($gmaiUserId){
			/*$data = $this->db
							->get_where($this->table_name, array('gmail_user_id' => $gmaiUserId))
							->group_by('thread_id');*/
							
			$data = $this->db
              ->select('*')
			  ->where(array('gmail_user_id' => $gmaiUserId))
              ->group_by('thread_id') 
              ->get($this->table_name);
//print_r($query->result());
			
			if ($data->num_rows() > 0) {
				foreach ($data->result_array() as $row) {
					$msgs[] = $row;
				}
			}
			$data->free_result();
		}else{
			$msgs = "";
		}

        return $msgs;
    }
	
	
}
